#!/bin/bash

export ARCH=arm
export CROSS_COMPILE=/opt/toolchains/bin/arm-eabi-
export USE_CCACHE=1
export KCONFIG_NOTIMESTAMP=true
export ENABLE_GRAPHITE=true

make ARCH=arm k3gxx_N_defconfig
make ARCH=arm -j8
